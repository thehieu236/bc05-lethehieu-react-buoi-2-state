import React, { Component } from 'react';

export default class ItemGlasses extends Component {
	handleItemGlasses = (id) => {
		return `glassesImage/g${id}.jpg`;
	};

	render() {
		let { id } = this.props.data;
		return (
			<div
				onClick={() => {
					this.props.handleMockupGlasses(id);
				}}
				className="glasses__item col-2">
				<img src={this.handleItemGlasses(id)} />
			</div>
		);
	}
}
