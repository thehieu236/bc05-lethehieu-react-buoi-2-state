import React, { Component } from 'react';
import { dataGlasses } from './dataGlasses';
import ListGlasses from './ListGlasses';
export default class TestGlasses extends Component {
	state = {
		glassesArr: dataGlasses,
	};

	handleMockupGlasses = (id) => {
		this.setState({
			img: `glassesImage/v${id}.png`,
			name: dataGlasses[id].name,
			price: dataGlasses[id].price,
			desc: dataGlasses[id].desc,
		});

		document.querySelector('.mockup').style.display = 'block';
	};

	render() {
		return (
			<>
				<h3>TRY GLASSES APP ONLINE</h3>
				<div className="container">
					<div id="avatar__glasses" className="row">
						<div className="col-4">
							<img src="./glassesImage/model.jpg" />
							<div className="mockup">
								<img className="glasses__mockup" src={this.state.img} />
								<div className="glasses__info">
									<div className="name__glasses">{this.state.name}</div>
									<span>{this.state.price}$</span>
									<p>{this.state.desc}</p>
								</div>
							</div>
						</div>

						<div className="col-4">
							<img src="./glassesImage/model.jpg" />
						</div>
					</div>
					<div className="list__glasses">
						<ListGlasses
							handleMockupGlasses={this.handleMockupGlasses}
							glassesArr={this.state.glassesArr}
						/>
					</div>
				</div>
			</>
		);
	}
}
