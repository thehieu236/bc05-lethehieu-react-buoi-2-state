import React, { Component } from 'react';
import ItemGlasses from './ItemGlasses';

export default class ListGlasses extends Component {
	renderListGlasses = () => {
		return this.props.glassesArr.map((item) => {
			return (
				<ItemGlasses
					handleMockupGlasses={this.props.handleMockupGlasses}
					data={item}
				/>
			);
		});
	};

	render() {
		return <div className="row">{this.renderListGlasses()}</div>;
	}
}
