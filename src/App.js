import './App.css';
import TestGlasses from './Glasses/TestGlasses';

function App() {
	return (
		<div className="App">
			<TestGlasses />
		</div>
	);
}

export default App;
